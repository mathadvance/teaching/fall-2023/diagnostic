\documentclass{marticl}

\usepackage{newpxtext,newpxmath}

\usepackage{asymptote}
	\begin{asydef}
		usepackage("newpxtext, newpxmath");
	\end{asydef}

\title{AIME Course Diagnostic}
\author{Math Advance}
\date{Fall 2023}

\begin{document}

\maketitle

Tuition, if accepted, is \emph{100 US dollars} for 10 lectures in the Fall 2023 semester. \emph{UPDATE}: The deadline has been extended from September 3rd to \emph{September 6th}.

\section{Instructions}

Unlike past courses taught by Math Advance, this time the diagnostic is truly a diagnostic. This means we will only make recommendations (based on diagnostic submissions) whether a student is prepared for this course, unprepared, or (rarely) that they are beyond the level of this course and thus would gain little from it.

It is not necessary to ``pass'' the diagnostic; if you believe you are ready for the course despite our recommendations, you will be permitted to attend regardless. Despite this, \emph{it is still mandatory to complete it and send complete solutions}. To apply, you should email teaching@mathadvance.org the contents described in the following paragraph, with the subject line \emph{Fall 2023 Application: FIRSTNAME LASTNAME}. A valid subject line might look like ``Fall 2023 Application: Dennis Chen''.

Please send the following information \emph{in this order} in your email.

\begin{enumerate}
	\item Country and state (if in the US)

	\item Relevant scores in past contests, if any. Whether a score is ``relevant'' is left to your judgment, but generally you should include at least your latest AMC 10/12 scores and AIME score (if applicable).
	
	\item \emph{Whether financial aid is required.} This is important. Please read \url{https://dennisc.net/writing/blog/finaid} for our financial aid policy; we are very liberal in granting financial aid, no questions asked. If you think you need financial aid you should ask for it. Price will not serve as a barrier to accessing this course; that is our commitment.
	
	\item Attach a PDF of your solutions to this diagnostic. It is recommended you typeset your solutions with LaTeX. Otherwise, please write your solutions on a tablet with an app like Saber. Scans of handwritten solutions are strongly discouraged.
\end{enumerate}

\section{Rules}

We afford discretion to the student to seek assistance with diagnostic questions. However, our policy is that students must honestly disclose any assistance they receive, as well whether they have seen any of these questions in the past. Here are a couple of guidelines of what kinds of help will best help in assessing your preparedness, as well as best help you learn.

\begin{enumerate}
	\item All questions on the diagnostic are past contest questions. Most of them are from the MAT. It is one thing if you have already looked at solutions to certain MAT questions before starting this application. However, we strongly recommend you do not proactively search for questions after you are aware they are on this diagnostic.

	\item If you ask someone (besides the Math Advance team) for help, you should make it clear that this is a diagnostic and that they should give you a hint to lead you in the right direction, rather than a complete solution. \emph{If someone has helped you, you should clearly note it on your application.} Briefly paraphrase the hints you have been given on each question.

	\item If you do not know who to ask for help, you should ask us. Email teaching@mathadvance.org with the subject line \emph{Fall 2023 Hint Request: FIRSTNAME LASTNAME}. If you have received a hint from us, you should still note it in your application to remind us.

	\item If you have solved one of these questions, whether in a past contest or just when practicing, that is fine. We encourage prospective students to take our contests. You just need to make a note of the form, ``I solved this question when taking the respective MAT'', ``I attempted this question during the MAT and looked up the solution afterwards'', ``I solved this question on my own time'', etc.
\end{enumerate}

\section{Questions}

\subsection*{Algebra}

\begin{prob}
	While in math class, Anthony's teacher displays the expression
	\[(a+7)\times (b+2),\]
	where $a$ and $b$ are positive integers known to Anthony, and asks him to compute it. Since Anthony's vision is poor, he misreads $+$ signs as $\times$ signs and $\times$ signs as $+$ signs. However, he coincidentally arrives at the correct answer. What is the sum of all possible values of $a+b$?
\end{prob}

\begin{prob}
	Let $a, b$, and $c$ be distinct positive integers with $a + b + c = 10$. Then there exists a quadratic polynomial $p$ satisfying $p(a) = bc$, $p(b) = ac$, and $p(c) = ab$. Find the maximum possible value of $p(10)$.
\end{prob}

\subsection*{Combinatorics}

\begin{prob}
How many ways can you color at least one square out of the five below black such that no two black squares share a side?
	\begin{center}
		\begin{asy}
			size(4cm);
			draw((1,0)--(1,3));
			draw((2,0)--(2,3));
			draw((0,1)--(3,1));
			draw((0,2)--(3,2));
			draw((1,0)--(2,0));
			draw((1,3)--(2,3));
			draw((0,1)--(0,2));
			draw((3,1)--(3,2));
		\end{asy}
	\end{center}
\end{prob}

\begin{prob}
	Five people are in a group, including Kai and Wen. To exercise \emph{social distancing}, they keep dividing their groups into two disjoint groups until no two people are in a group. Because of a long-standing grudge, Kai and Wen cannot be together in a group after the first division. If the order of divisions matters and divisions occur one at a time, how many possible processes exist?
\end{prob}

\subsection*{Geometry}

\begin{prob}
	  In the figure below, a plus sign is formed with five congruent squares. A shaded square with area $11$ is placed inside the plus sign such that the inner vertices of the plus sign lie on the sides of the square, as shown. The indicated triangles all have area $4$. Find the area of the plus sign.
	\begin{center}
		\begin{asy}
			size(5cm);
			pair A = (0, 0), B = (1, 0), C = (1, 1), D = (0, 1);
			pair E = (1, -1), F = (0, -1), G = (0, 2), H = (1, 2);
			pair I = (2, 1), J = (2, 0), K = (-1, 1), L = (-1, 0);
			pair P1 = (-0.9, 0.3), P2 = P1 * dir(90), P3 = P2 * dir(90), P4 = P3 * dir(90);
			pair R1 = P1 + (0.5, 0.5), R2 = P2 + (0.5, 0.5), R3 = P3 + (0.5, 0.5), R4 = P4 + (0.5, 0.5);
			
			draw(B--E--F--A);
			draw(D--G--H--C);
			draw(C--I--J--B);
			draw(A--L--K--D);
			filldraw(R1--R2--R3--R4--cycle, lightgrey);
			filldraw(R1--L--K--cycle, white);
			filldraw(R4--G--H--cycle, white);
			filldraw(R3--I--J--cycle, white);
			filldraw(R2--E--F--cycle, white);
			label((R1+L+K)/3, "$4$");
			label((R4+G+H)/3, "$4$");
			label((R3+I+J)/3, "$4$");
			label((R2+E+F)/3, "$4$");
			label((R1+R2+R3+R4)/4, "$11$");
		\end{asy}
	\end{center}
\end{prob}

\begin{prob}
	 In rectangle $ABCD$, $M$ is the midpoint of $CD$. The circle centered at $B$ passing through $A$ intersects the circle centered at $M$ passing through $C$ at $E$ and $F$. If $E$, $M$, and $F$ are collinear and $AB=30$, find $BC^2$.
\end{prob}

\subsection*{Number Theory}

\begin{prob}
	For how many integers $n$ is $\frac n{20-n}$ the square of an integer?
\end{prob}

(I sincerely apologize I could not find a better problem.)

\begin{prob}
	Define a sequence $x_n$ with $x_0=1$, and have $x_n$ be the result when $2^n$ is appended to $x_{n-1}$. (For example, $x_1=12$, $x_2=124$, $x_3=1248$, and $x_4=124816$.) Let $k_n$ be the largest integer such that $2^{k_n}$ divides $x_n$. What is $k_0 + k_1 + \cdots + k_{20}$?
\end{prob}

\end{document}
